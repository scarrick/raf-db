/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb;

import com.pekinsoft.rafdb.db.RecordReader;
import com.pekinsoft.rafdb.db.RecordWriter;
import com.pekinsoft.rafdb.db.RecordsFile;
import com.pekinsoft.rafdb.exceptions.RecordsFileException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Sean Carrick
 */
public class RAFDatabaseSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<Person> people = new ArrayList<>();
        Person person = new Person();
        
        people.add(person); person = new Person();
        
        person.setCity("Johnstown");
        person.setName("Mack");
        person.setPhone("(111) 222-3333");
        person.setState("RI");
        people.add(person); person = new Person();
        
        person.setCity("Mackinaw");
        person.setName("Skippy");
        person.setPhone("(222) 333-4444");
        person.setState("CT");
        people.add(person);
        
        try {
            
            File f = new File("people.jdb");
            
            try {
                RecordsFile recordsFile = null;
                // Testing the API:
                
                if ( !f.exists() ) {
                    recordsFile = new RecordsFile("people.jdb", 3);
                
                    for (Person p : people) {
                        RecordWriter rw = new RecordWriter("name." + p.getName());
                        rw.writeObject(p);
                        recordsFile.insertRecord(rw);
                    }

                    recordsFile.close();
                }
            } catch (IOException | RecordsFileException ex) {
                ex.printStackTrace(System.err);
            }
            
            people.clear();
            
            RecordsFile rf = new RecordsFile("people.jdb", "rw");
            
            for ( int x = 1; x < rf.getNumRecords(); x++ ) {
                RecordReader rr = rf.readRecord("name." + person.getName());
                people.add((Person)rr.readObject());
            }
            
            for (Person p : people) { 
                System.out.println("Person: " + p.toString());
            }
            
            rf.close();
            } catch (IOException | RecordsFileException | ClassNotFoundException ex) {
                ex.printStackTrace(System.err);
        }
    }
    
}
