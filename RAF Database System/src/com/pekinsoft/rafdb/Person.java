/*
 * Copyright (C) 2019 Carrick Trucking
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb;

import java.io.Serializable;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class Person implements Serializable {
    static final long serialVersionUID = 16L;
    private String name;
    private String city;
    private String state;
    private String phone;
    
    public Person() {
        name = "Tommy";
        city = "Pekin";
        state = "IL";
        phone = "(309) 346-1234";
    }
    
    public String toString() {
        return name + " from " + city + ", " + state + " :: PH# " + phone;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
