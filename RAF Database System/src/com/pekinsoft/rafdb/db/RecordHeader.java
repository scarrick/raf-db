/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import com.pekinsoft.rafdb.exceptions.RecordsFileException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 *
 * @author Sean Carrick
 */
public class RecordHeader {
/**
     * File pointer to the first byte of record data (8 bytes)
     */
    protected long dataPointer;
    /**
     * Actual number of bytes of data held in this record (4 bytes)
     */
    protected int dataCount;
    /**
     * Number of bytes of data that this record can hold (4 bytes)
     */
    protected int dataCapacity;
    /**
     * Indicates this header's position in the file index.
     */
    protected int indexPosition;
    
    /**
     * Default constructor.
     */
    protected RecordHeader() {
        
    }

    /**
     * 
     * @param dataPointer
     * @param dataCapacity 
     */
    protected RecordHeader(long dataPointer, int dataCapacity) {
        if ( dataCapacity < 1 ) 
            throw new IllegalArgumentException("Bad record size: " + 
                                                                  dataCapacity);
        
        this.dataPointer = dataPointer;
        this.dataCapacity = dataCapacity;
        this.dataCount = 0;
    }
    
    /**
     * Retrieves the index position of this {@code RecordHeader} object.
     * 
     * @return int The index position.
     */
    protected int getIndexPosition() {
        return indexPosition;
    }
    
    /**
     * Sets the index position of this {@code RecordHeader} object.
     * 
     * @param indexPosition The new index position.
     */
    protected void setIndexPosition(int indexPosition) {
        this.indexPosition = indexPosition;
    }
    
    /**
     * Retrieves the data capacity of this {@code RecordHeader} object.
     * 
     * @return int The data capacity.
     */
    protected int getDataCapacity() {
        return dataCapacity;
    }
    
    /**
     * Retrieves the data pointer to the location in the file that this header's
     * data begins.
     * 
     * @return long The start of this record's data in the file.
     */
    protected long getDataPointer() {
        return dataPointer;
    }
    
    /**
     * Discovers how much free space is available for data to be added.
     * 
     * @return int The available free space for data.
     */
    protected int getFreeSpace() {
        return dataCapacity - dataCount;
    }
    
    /**
     * Reads the parameters of this {@code RecordHeader} object to learn the:
     * <ul>
     *  <li>Location of the data pointer</li>
     *  <li>Data capacity for this header</li>
     *  <li>Size for data in this header</li>
     * </ul>
     * 
     * @param in            The {@code DataInput} object to use to read the 
     *                      values.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected void read(DataInput in) throws IOException {
        dataPointer = in.readLong();
        dataCapacity = in.readInt();
        dataCount = in.readInt();
    }
    
    /**
     * Writes the parameters of this {@code RecordHeader} object to the file.
     * 
     * @param out           The {@code DataOutput} object to use to write the 
     *                      values.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected void write(DataOutput out) throws IOException {
        out.writeLong(dataPointer);
        out.writeInt(dataCapacity);
        out.writeInt(dataCount);
    }
    
    /**
     * Creates a new {@code RecordHeader} object from an existing 
     * {@code RecordHeader} on disk.
     * 
     * @param in            The {@code DataInput} object to use to read in the
     *                      {@code RecordHeader} from file.
     * @return RecordHeader The record header retrieved from file.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected static RecordHeader readHeader(DataInput in) throws IOException {
        RecordHeader r = new RecordHeader();
        r.read(in);
        return r;
    }
    
    /**
     * Returns a new {@code RecordHeader} object, which occupies the free space 
     * of this record. It also shrinks this record's size by the size of its
     * free space.
     * 
     * @return RecordHeader          The new record header from the free space.
     * @throws RecordsFileException  In the event a database error occurs.
     */
    protected RecordHeader split() throws RecordsFileException {
        long newFp = dataPointer + (long)dataCount;
        RecordHeader newRecord = new RecordHeader(newFp, getFreeSpace());
        dataCapacity = dataCount;
        return newRecord;
    }
}
