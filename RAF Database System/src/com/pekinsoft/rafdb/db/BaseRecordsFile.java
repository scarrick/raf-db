/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import com.pekinsoft.rafdb.exceptions.RecordsFileException;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Enumeration;

/**
 *
 * @author Sean Carrick
 */
public abstract class BaseRecordsFile {
    //<editor-fold defaultstate="collapsed" desc="Private Fields">
    // The database file.
    private RandomAccessFile file;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Protected Fields">
    // Current file pointer to the start of the record data.
    protected long dataStartPtr;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Protected Constants">
    // Total length, in bytes, of the global database headers.
    protected static final int FILE_HEADERS_REGION_LENGTH = 16;
    // Number of bytes in the record header.
    protected static final int RECORD_HEADER_LENGTH = 16;
    // The length of a key in the index.
    protected static final int MAX_KEY_LENGTH = 64;
    // The total length of one index entry: the key length plus the record 
    //+ header length.
    protected static final int INDEX_ENTRY_LENGTH = MAX_KEY_LENGTH + 
                                                    RECORD_HEADER_LENGTH;
    // File pointer to the num records header.
    protected static final long NUM_RECORDS_HEADER_LOCATION = 0;
    // File pointer to the data start pointer header.
    protected static final long DATA_START_HEADER_LOCATION = 4;
    //</editor-fold>

    /**
     * Creates a new database file, initializing the appropriate headers.Enough
     * space is allocated in the index for the specified file size.
     * 
     * @param dbPath    The path to the database file you wish to create.
     * @param initialSize   The initial size of the database file.
     * @throws java.io.IOException  In the event of an Input/Output error.
     * @throws com.pekinsoft.rafdb.exceptions.RecordsFileException In the event of
     *                  an error with the database file itself.
     */
    protected BaseRecordsFile(
                                String dbPath, 
                                int initialSize
                            ) throws 
                                IOException,
                                RecordsFileException {
        // Create the file that we are going to use.
        File f = new File(dbPath);
        
        // Test whether the file already exists.
        if ( f.exists() )
            throw new RecordsFileException("Database already exists: " + dbPath);
        
        file = new RandomAccessFile(f, "rw");
        
        // Record Data Region starts where the (i=1)th index entry would start.
        dataStartPtr = indexPositionToKeyFp(initialSize);
        setFileLength(dataStartPtr);
        writeNumRecordsHeader(0);
        writeDataStartPtrHeader(dataStartPtr);
    }
    
    /**
     * Opens an existing database file and initializes the dataStartPtr. The 
     * {@code accessFlags} parameter can be "r" or "rw" &mdash; as defined in
     * {@code RandomAccessFile}.
     * 
     * @param dbPath    The path to the database file you wish to open.
     * @param accessFlags   Determines whether database file is opened in 
     *                      read-only or read/write mode.
     * @throws IOException  In the event of an Input/Output error.
     * @throws RecordsFileException In the event of
     *                  an error with the database file itself.
     */
    protected BaseRecordsFile(String dbPath, String accessFlags) throws
                                IOException, RecordsFileException {
        // Create the file that we are going to use.
        File f = new File(dbPath);
        
        // Test whether the file already exists.
        if ( !f.exists() )
            throw new RecordsFileException("Database not found: " + dbPath);
        
        file = new RandomAccessFile(f, accessFlags);
        dataStartPtr = readDataStartHeader();
    }
    
    /**
     * Returns an Enumeration of the keys of all records in the database.
     * 
     * @return Enumeration All keys in the database.
     */
    public abstract Enumeration enumerateKeys();
    
    /**
     * Returns the number of records in the database.
     * 
     * @return int Number of records.
     */
    public abstract int getNumRecords();
    
    /**
     * Check there is a record with the given key.
     * 
     * @param key The key to check.
     * @return boolean {@code true} if the key exists, {@code false} otherwise.
     */
    public abstract boolean recordExists(String key);
    
    /**
     * Maps a key to a record header.
     * 
     * @param key The key to map.
     * @return RecordHeader containing the map to the record for the given key.
     */
    protected abstract RecordHeader keyToRecordHeader(String key) throws
                                RecordsFileException;
    
    /**
     * Locates space for a new record of {@code dataLength} size and initializes
     * a {@code RecordHeader}.
     * 
     * @param key The key for the record.
     * @param dataLength The length of the data record.
     * @throws IOException In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event of a database error.
     */
    protected abstract RecordHeader allocateRecord(String key, int dataLength)
                                throws IOException, RecordsFileException;
    
    /**
     * Returns the record to which the target file pointer belongs &mdash;
     * meaning the specified location in the file is part of the record data of
     * the {@code RecordHeader} which is returned. (O(n) mem accesses)
     * 
     * @param targetFp The target file pointer for the record to retrieve.
     * @return RecordHeader The header for the record represented by the
     *                      file pointer provided.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected abstract RecordHeader getRecordAt(long targetFp) throws 
                                RecordsFileException;
    
    /**
     * Gets the length of the file. Used to determine whether the file is big
     * enough for the data or if it needs to be shrunk to save disk space.
     * 
     * @return int The current length of the file.
     * @throws IOException In the even of an Input/Output error occurs.
     */
    protected long getFileLength() throws IOException {
        return file.length();
    }

    /**
     * Sets the file to the length provided. Used to grow the file as necessary
     * and to initialize a new file.
     * 
     * @param len           Requested length of the file.
     * @throws IOException  In the event of an Input/Output error occurs.
     */
    protected void setFileLength(long len) throws IOException {
        file.setLength(len);
    }
    
    /**
     * Retrieves the number of records in the database file.
     * 
     * @return int          The number of records currently in the database file.
     * @throws IOException  In the event of an Input/Output error occurs.
     */
    protected int readNumRecordsHeader() throws IOException {
        file.seek(NUM_RECORDS_HEADER_LOCATION);
        return file.readInt();
    }

    /**
     * Stores the number of records to the database file.
     * 
     * @param numRecords    The number of records in the database file.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected void writeNumRecordsHeader(int numRecords) throws IOException {
        file.seek(NUM_RECORDS_HEADER_LOCATION);
        file.writeInt(numRecords);
    }

    /**
     * Reads the data start pointer header from the file.
     * 
     * @return long         The stored value of the data start location in the
     *                      database file.
     * @throws IOException In the event of an Input/Output error occurs.
     */
    protected long readDataStartHeader() throws IOException {
        file.seek(DATA_START_HEADER_LOCATION);
        return file.readLong();
    }

    /**
     * Writes the data start pointer header to the file.
     * 
     * @param dataStartPtr  The new data start pointer to store.
     * @throws IOException  In the event of an Input/Output error occurs.
     * @param dataStartPtr 
     */
    protected void writeDataStartPtrHeader(long dataStartPtr) throws IOException {
        file.seek(DATA_START_HEADER_LOCATION);
        file.writeLong(dataStartPtr);
    }
    
    /**
     * Returns a file pointer in the index pointing to the first byte in the key
     * located at the given index position.
     * 
     * @param pos Position in the file.
     * @return The file pointer to the index of the key.
     */
    protected long indexPositionToKeyFp(int pos) {
        return FILE_HEADERS_REGION_LENGTH + (INDEX_ENTRY_LENGTH * pos);
    }
    
    /**
     * Returns a file pointer in the index pointing to the first byte in the
     * record pointer located at the given index position.
     * 
     * @param pos           The index position from which to retrieve the file
     *                      pointer.
     * @return long         The file pointer of the index position requested.
     */
    long indexPositionToRecordHeaderFp(int pos) {
        return FILE_HEADERS_REGION_LENGTH + (INDEX_ENTRY_LENGTH * pos);
    }
    
    /**
     * Reads the ith key from the index.
     * 
     * @param position      The position in the index for the key.
     * @return String       The requested key from the index.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    String readKeyFromIndex(int position) throws IOException {
        file.seek(indexPositionToKeyFp(position));
        return file.readUTF();
    }
    
    /**
     * Reads the ith record header from the index.
     * 
     * @param position      The position in the index for the header.
     * @return RecordHeader The requested record header from the index.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    RecordHeader readRecordHeaderFromIndex(int position) throws IOException {
        file.seek(indexPositionToRecordHeaderFp(position));
        return RecordHeader.readHeader(file);
    }
    
    /**
     * Writes the ith record header to the index.
     * 
     * @param header        The RecordHeader object to write.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected void writeRecordHeaderToIndex(RecordHeader header) throws
                                IOException {
        file.seek(indexPositionToRecordHeaderFp(header.indexPosition));
        header.write(file);
    }
    
    /**
     * Appends an entry to the end of the index. Assumes that 
     * {@code insureIndexSpace()} has already been called.
     * 
     * @param key           The key for the index to append.
     * @param newRecord The new record header to append to the index.
     * @param currentNumRecords The current number of records in the database.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected void addEntryToIndex(String key, RecordHeader newRecord,
                                   int currentNumRecords) throws
                                   IOException, RecordsFileException {
        DbByteArrayOutputStream temp = new DbByteArrayOutputStream(MAX_KEY_LENGTH);
        (new DataOutputStream(temp)).writeUTF(key);
        
        if ( temp.size() > MAX_KEY_LENGTH )
            throw new RecordsFileException("Key is larger than permitted " + 
                                        "size of " + MAX_KEY_LENGTH + " bytes");
        
        file.seek(indexPositionToKeyFp(currentNumRecords));
        temp.writeTo(file);
        file.seek(indexPositionToRecordHeaderFp(currentNumRecords));
        newRecord.write(file);
        newRecord.setIndexPosition(currentNumRecords);
        writeNumRecordsHeader(currentNumRecords + 1);
    }
    
    /**
     * Removes the record from the index. Replaces the target with the entry at 
     * the end of the index.
     * 
     * @param key           The key for the index to remove.
     * @param header        The record header to remove.
     * @param currentNumRecords The current number of records in the database.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event of a database error.
     */
    protected void deleteEntryFromIndex(String key, RecordHeader header,
                                        int currentNumRecords) throws
                                        IOException, RecordsFileException {
        if ( header.indexPosition != currentNumRecords - 1 ) {
            String lastKey = readKeyFromIndex(currentNumRecords - 1);
            RecordHeader last = keyToRecordHeader(lastKey);
            last.setIndexPosition(header.indexPosition);
            file.seek(indexPositionToKeyFp(last.indexPosition));
            file.writeUTF(lastKey);
            file.seek(indexPositionToRecordHeaderFp(last.indexPosition));
            last.write(file);
        }
        
        writeNumRecordsHeader(currentNumRecords - 1);
    }
    
    /**
     * Adds a new record to the database.
     * 
     * @param rw            The new record to write to the database.
     * @throws IOException  In the event an Input/Output error occurs
     * @throws RecordsFileException In the event a database error occurs.
     */
    public synchronized void insertRecord(RecordWriter rw) throws IOException,
                                                          RecordsFileException {
        String key = rw.getKey();
        
        if ( recordExists(key) ) 
            throw new RecordsFileException("Key exists: " + key);
        
        insureIndexSpace(getNumRecords() + 1);
        
        RecordHeader newRecord = allocateRecord(key, rw.getDataLength());
        writeRecordData(newRecord, rw);
        addEntryToIndex(key, newRecord, getNumRecords());
    }
    
    
    public synchronized void quickInsert(RecordWriter rw) throws IOException,
                                                         RecordsFileException {
        String key = rw.getKey();
        
        insureIndexSpace(getNumRecords() + 1);
        
        RecordHeader rh = new RecordHeader(dataStartPtr, rw.getDataLength());
        writeRecordData(rh, rw);
        addEntryToIndex(key, rh, getNumRecords());
    }
    
    /**
     * Updates an existing record. If the new contents do not fit in the orginal
     * record, the the update is handled by deleting the old record and adding
     * the new.
     * 
     * @param rw            The record to update.
     * @throws IOException  In the event an Input/Output error occurs
     * @throws RecordsFileException In the event a database error occurs.
     */
    public synchronized void updateRecord(RecordWriter rw) throws IOException,
                                                          RecordsFileException {
        RecordHeader header = keyToRecordHeader(rw.getKey());
        
        if ( rw.getDataLength() > header.dataCapacity ) {
            deleteRecord(rw.getKey());
            insertRecord(rw);
        } else {
            writeRecordData(header, rw);
            writeRecordHeaderToIndex(header);
        }
    }
    
    /**
     * Reads the requested record from the database.
     * 
     * @param key           The record to retrieve from the database.
     * 
     * @return RecordReader The reader of the record object.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the even a database error occurs.
     */
    public synchronized RecordReader readRecord(String key) throws IOException,
                                                          RecordsFileException {
        byte[] data = readRecordData(key);
        return new RecordReader(key, data);
    }
    
    /**
     * Reads the data for the requested record from the database.
     * 
     * @param key           The key for the record whose data is to be read.
     * 
     * @return byte[]       The data from the requested record.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected byte[] readRecordData(String key) throws IOException,
                                                       RecordsFileException {
        return readRecordData(keyToRecordHeader(key));
    }
    
    /**
     * Reads the data for the requested record from the database.
     * 
     * @param header        The record header for the data to read.
     * 
     * @return byte[]       The data from the requested record.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     */
    protected byte[] readRecordData(RecordHeader header) throws IOException {
        byte[] buf = new byte[header.dataCount];
        file.seek(header.dataPointer);
        file.readFully(buf);
        return buf;
    }
    
    /**
     * Updates the contents of the given record.A {@code RecordsFileException}
     * is thrown if the new data does not fit in the space allocated to the
     * record.The header's data count is updated, but not written to the file.
     * 
     * @param header        The header of the record to write to the file.
     * @param rw            The {@code RecordWriter} object to use to write the
     *                      data to the file.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected void writeRecordData(RecordHeader header, RecordWriter rw) throws
                                            IOException, RecordsFileException {
        if ( rw.getDataLength() > header.dataCapacity )
            throw new RecordsFileException("Record data does not fit.");
        
        header.dataCount = rw.getDataLength();
        file.seek(header.dataPointer);
        rw.writeTo((DataOutput)file);
    }
    
    /**
     * Updates the contents of the given record. A {@code RecordsFileException}
     * is thrown if the data does not fit in the space allocated to the record.
     * The header's data count is updated, but not written to the file.
     * 
     * @param header        The header of the record to update.
     * @param data          The data to update into the record.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected void writeRecordData(RecordHeader header, byte[] data) throws
                                            IOException, RecordsFileException {
        if ( data.length > header.dataCapacity )
            throw new RecordsFileException("Record data does not fit.");
        
        header.dataCount = data.length;
        file.seek(header.dataPointer);
        file.write(data, 0, data.length);
    }
    
    /**
     * Deletes a record from the database.
     * 
     * @param key           The key of the record to delete.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    public synchronized void deleteRecord(String key) throws IOException,
                                                          RecordsFileException {
        RecordHeader delRec = keyToRecordHeader(key);
        int currentNumRecords = getNumRecords();
        if ( getFileLength() == delRec.dataPointer + delRec.dataCapacity )
            // Shrink the file since this is the last record in it.
            setFileLength(delRec.dataPointer);
        else {
            RecordHeader previous = getRecordAt(delRec.dataPointer - 1);
            
            if ( previous != null ) {
                // Append space of deleted record onto previous record.
                previous.dataCapacity += delRec.dataCapacity;
                
                writeRecordHeaderToIndex(previous);
            } else {
                // Target record is first in the file and is deleted by adding
                //+ its space to the second record.
                RecordHeader secondRecord = getRecordAt(delRec.dataPointer + 
                                                    (long) delRec.dataCapacity);
                
                byte[] data = readRecordData(secondRecord);
                
                secondRecord.dataPointer = delRec.dataPointer;
                secondRecord.dataCapacity += delRec.dataCapacity;
                
                writeRecordData(secondRecord, data);
                writeRecordHeaderToIndex(secondRecord);
            }
        }
        
        deleteEntryFromIndex(key, delRec, currentNumRecords);
    }
    
    /**
     * Checks to see if there is space for an additional index entry. If not,
     * space is created by moving records to the end of the file.
     * 
     * @param requiredNumRecords    The required number of records.
     * 
     * @throws IOException          In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    protected void insureIndexSpace(int requiredNumRecords) throws IOException,
                                                        RecordsFileException {
        int currentNumRecords = getNumRecords();
        long endIndexPtr = indexPositionToKeyFp(requiredNumRecords);
        
        if ( endIndexPtr > getFileLength() && currentNumRecords == 0 ) {
            setFileLength(endIndexPtr);
            dataStartPtr = endIndexPtr;
            
            writeDataStartPtrHeader(dataStartPtr);
            
            return;
        }
        
        while ( endIndexPtr > dataStartPtr ) {
            RecordHeader first = getRecordAt(dataStartPtr);
            byte[] data = readRecordData(first);
            
            first.dataPointer = getFileLength();
            first.dataCapacity = data.length;
            
            setFileLength(first.dataPointer + data.length);
            
            writeRecordData(first, data);
            writeRecordHeaderToIndex(first);
            
            dataStartPtr += first.dataCapacity;
            writeDataStartPtrHeader(dataStartPtr);
        }
    }
    
    /**
     * Closes the file.
     * 
     * @throws IOException          In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs
     */
    public synchronized void close() throws IOException, RecordsFileException {
        try {
            file.close();
        } finally {
            file = null;
        }
    }
}
