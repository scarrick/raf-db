/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;

/**
 *
 * @author Sean Carrick
 */
public class RecordReader {
    String key;
    byte[] data;
    ByteArrayInputStream in;
    ObjectInputStream objIn;
    
    /**
     * Creates a new {@code RecordReader} object to use for reading in the 
     * records from the database file.
     * 
     * @param key   The key of the record to read.
     * @param data  the data of the record to read.
     */
    public RecordReader(String key, byte[] data) {
        this.key = key;
        this.data = data;
        in = new ByteArrayInputStream(data);
    }
    
    /**
     * Retrieves the key for this record.
     * 
     * @return String   Containing the record's key.
     */
    public String getKey() {
        return key;
    }
    
    /**
     * Retrieves the data for this record.
     * 
     * @return byte[]   Containing the data.
     */
    public byte[] getData() {
        return data;
    }
    
    /**
     * Gets an {@code InputStream} object for reading fields from the record.
     * The user should use <em>either</em> this method <strong>or</strong> the
     * {@code getObjectInputStream} method for reading data &mdash; mixed access
     * will produce undefined results.
     * 
     * @return InputStream  Stream for reading data fields.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    public InputStream getInputStream()  throws IOException {
        return in;
    }
    
    /**
     * Gets an {@code ObjectInputStream} for reading serialized objects. The
     * user should use <em>either</em> this method <strong>or</strong> the 
     * {@code getInputStream} method for reading data &mdash; mixed access will
     * produce undefined results.
     * 
     * @return ObjectInputStream    Stream for reading serialized objects.
     * @throws IOException          In the event an Input/Output error occurs.
     */
    public ObjectInputStream getObjectInputStream() throws IOException {
        if ( objIn == null )
            objIn = new ObjectInputStream(in);
        
        return objIn;
    }
    
    /**
     * Reads the next object in the record using an {@code ObjectInputStream}.
     * 
     * @return Object                   The next object in the record.
     * @throws IOException              In the event an Input/Output error occurs.
     * @throws OptionalDataException    In the event a data error occurs.
     * @throws ClassNotFoundException   In the event the class cannot be found.
     */
    public Object readObject() throws IOException, 
                                      OptionalDataException, 
                                      ClassNotFoundException {
        return getObjectInputStream().readObject();
    }
}
