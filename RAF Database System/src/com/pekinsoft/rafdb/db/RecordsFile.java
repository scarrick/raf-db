/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import com.pekinsoft.rafdb.exceptions.RecordsFileException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Sean Carrick
 */
public class RecordsFile extends BaseRecordsFile {
    
    /**
     * Hashtable which holds the in-memory index. For efficiency, the entire
     * index is cached in memory. The hashtable maps a key of type {@code String}
     * to a {@code RecordHeader}.
     */
    protected ConcurrentHashMap memIndex;
    
    /**
     * Creates a new database file. The index can grow dynamically, but an 
     * initial size needs to be provided.
     * 
     * @param dbPath        The path to the database file to be created.
     * @param initialSize   The initial size that should be set for the file.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    public RecordsFile(String dbPath, int initialSize) throws IOException,
                                                          RecordsFileException {
        super(dbPath, initialSize);
        memIndex = new ConcurrentHashMap(initialSize);
    }
    
    /**
     * Opens an existing database file and initializes the in-memory index.
     * 
     * @param dbPath        The path to the database file to open.
     * @param accessFlags   The accessibility flags for the file. "r" for read-
     *                      only or "rw" for read/write.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    public RecordsFile(String dbPath, String accessFlags) throws IOException,
                                                          RecordsFileException {
        super(dbPath, accessFlags);
        
        int numRecords = readNumRecordsHeader();
        memIndex = new ConcurrentHashMap(numRecords);
        
        for ( int i = 0; i < numRecords; i++ ) {
            String key = readKeyFromIndex(i);
            RecordHeader header = readRecordHeaderFromIndex(i);
            header.setIndexPosition(i);
            memIndex.put(key, header);
        }
    }

    /**
     * Returns an {@code Enumeration} of all the keys in the database.
     * 
     * @return Enumeration All of the keys.
     */
    @Override
    public Enumeration enumerateKeys() {
        return memIndex.keys();
    }

    /**
     * Returns the current number of records in the database.
     * 
     * @return int The total number of records.
     */
    @Override
    public int getNumRecords() {
        return memIndex.size();
    }

    /**
     * Checks if there is a record belonging to the given key.
     * 
     * @param key       The key to check.
     * @return boolean  {@code true} if the key exists; {@code false} otherwise.
     */
    @Override
    public boolean recordExists(String key) {
        return memIndex.containsKey(key);
    }

    /**
     * Maps a key to a record header by looking it up in the in-memory index.
     * 
     * @param key       The key to map to a header.
     * @return RecordHeader The header that was mapped to the key.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    protected RecordHeader keyToRecordHeader(String key) throws RecordsFileException {
        RecordHeader h = (RecordHeader) memIndex.get(key);
        
        if ( h == null )
            throw new RecordsFileException("Key not found: " + key);
        
        return h;
    }

    /**
     * Searches the file for free space and then returns a {@code RecordHeader}
     * which uses the space. (0(n) memory accesses)
     * 
     * @param key           The key to use to locate space.
     * @param dataLength    The length of the data.
     * @return RecordHeader The record header created to use the space.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    protected RecordHeader allocateRecord(String key, int dataLength) throws IOException, RecordsFileException {
        RecordHeader newRecord = null;
        Enumeration e = memIndex.elements();
        
        while ( e.hasMoreElements() ) {
            RecordHeader next = (RecordHeader) e.nextElement();
            int free = next.getFreeSpace();
            
            if ( dataLength <= free ) {
                newRecord = next.split();
                writeRecordHeaderToIndex(next);
                break;
            }
        }
        
        if ( newRecord == null ) {
            // Append record to end of file - grows file to allocate space.
            long fp = getFileLength();
            setFileLength(fp + dataLength);
            newRecord = new RecordHeader(fp, dataLength);
        }
        
        return newRecord;
    }

    /**
     * Returns the record to which the target file pointer belongs - meaning the
     * specified location in the file is part of the record data of the
     * {@code RecordHeader} which is returned. Returns {@code null} if the
     * location is not part of a record. (0(n) mem accesses)
     * 
     * @param targetFp          The file pointer of the target record.
     * @return RecordHeader     The record header of the target record.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    protected RecordHeader getRecordAt(long targetFp) throws RecordsFileException {
        Enumeration e = memIndex.elements();
        
        while ( e.hasMoreElements() ) {
            RecordHeader next = (RecordHeader) e.nextElement();
            if ( targetFp >= next.dataPointer && targetFp < next.dataPointer +
                                                 (long) next.dataCapacity ) 
                return next;
        }
        
        return null;
    }
    
    /**
     * Closes the database.
     * 
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    public synchronized void close() throws IOException, RecordsFileException {
        try {
            super.close();
        } finally {
            memIndex.clear();
            memIndex = null;
        }
    }
    
    /**
     * Adds the new record to the in-memory index and calls the super class to
     * add the index entry to the file.
     * 
     * @param key           Key of the record to add.
     * @param newRecord     The record to add.
     * @param currentNumRecords The current number of records in the database.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    protected void addEntryToIndex(String key, RecordHeader newRecord,
                                    int currentNumRecords) throws IOException,
                                                        RecordsFileException {
        super.addEntryToIndex(key, newRecord, currentNumRecords);
        memIndex.put(key, newRecord);
    }
    
    /**
     * Removes the record from the index. Replaces the target with the entry at 
     * the end of the index.
     * 
     * @param key           The key of the record to delete.
     * @param header        The record header of the record to delete.
     * @param currentNumRecords The current number of records.
     * @throws IOException  In the event an Input/Output error occurs.
     * @throws RecordsFileException In the event a database error occurs.
     */
    @Override
    protected void deleteEntryFromIndex(String key, RecordHeader header,
                                    int currentNumRecords) throws IOException,
                                                        RecordsFileException {
        super.deleteEntryFromIndex(key, header, currentNumRecords);
        RecordHeader deleted = (RecordHeader) memIndex.remove(key);
    }
}
