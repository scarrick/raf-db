/*
 * Copyright (C) 2019 Integrity Solutions
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 *
 * @author Sean Carrick
 */
public class RecordWriter {
    String key;
    DbByteArrayOutputStream out;
    ObjectOutputStream objOut;
    
    /**
     * Default construction for the `RecordWriter` class.
     * 
     * @param key Key for the record to write.
     */
    public RecordWriter(String key) {
        this.key = key;
        out = new DbByteArrayOutputStream();
    }
    
    /**
     * Gets the key for this {@code RecordWriter} object.
     * 
     * @return String   The record writer's key.
     */
    public String getKey() {
        return key;
    }
    
    /**
     * Gets an {@code OutputStream} object for storing the record to file.
     * 
     * @return OutputStream     Object to use for storage.
     */
    public OutputStream getOutputStream() {
        return out;
    }
    
    /**
     * Gets an {@code ObjectOutputStream} that can be used to store serialized
     * objects in the record.
     * 
     * @return ObjectOutputStream   The stream to use to store the data.
     * @throws IOException          In the event an Input/Output error occurs.
     */
    public ObjectOutputStream getObjectOutputStream() throws IOException {
        if ( objOut == null)
            objOut = new ObjectOutputStream(out);
        
        return objOut;
    }
    
    /**
     * This utility method serializes an {@code Object} to the record.
     * 
     * @param o             {@code Object} to serialize.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    public void writeObject(Object o) throws IOException {
        getObjectOutputStream().writeObject(o);
        getObjectOutputStream().flush();
    }
    
    /**
     * Retrieves the number of bytes in the data.
     * 
     * @return int Number of data bytes.
     */
    public int getDataLength() {
        return out.size();
    }
    
    /**
     * Writes the data out to the stream without re-allocating the buffer.
     * 
     * @param str           The {@code DataOutput} object to which to write.
     * @throws IOException  In the event an Input/Output error occurs.
     */
    public void writeTo(DataOutput str) throws IOException {
        out.writeTo(str);
    }
}
