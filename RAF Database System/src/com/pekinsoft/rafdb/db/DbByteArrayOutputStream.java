/*
 * Copyright (C) 2019 Carrick Trucking
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.pekinsoft.rafdb.db;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.IOException;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class DbByteArrayOutputStream extends ByteArrayOutputStream {
    
    /**
     * Default constructor for the class.
     */
    public DbByteArrayOutputStream() {
        super();
    }
    
    /**
     * Constructor to initialize the class with a default size.
     * 
     * @param size The size to use for initialization.
     */
    public DbByteArrayOutputStream(int size) {
        super(size);
    }
    
    /**
     * @inheritDoc 
     */
    public synchronized void writeTo(DataOutput dstr) throws IOException {
        byte[] data = super.buf;
        int l = super.size();
        dstr.write(data, 0, l);
    }
}
